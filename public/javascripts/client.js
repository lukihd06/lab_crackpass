const product = require('cartesian-product');

// all the type of caracter we can use.
const lowerCase = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
let upperCase = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
let number = ['0','1','2','3','4','5','6','7','8','9']
let symbol = ['!','#','$','%','&','(',')','*','+',',','-','.','/',':',';','<','=','>','?','@','[',']','^','_','`','{','|','}','~']

let btnStart = document.getElementById("startBruteForce")
let chkLowercase = document.getElementById("lowercase")
let chkUppercase = document.getElementById("uppercase")
let chkSymbol = document.getElementById("symbol")
let chkNumber = document.getElementById("number")

let chekedPossibilities = []

function checkStatus() {
        // First we checked if checkbox status is at false 
    // then we delete the list if it exist in the Possibilities list
    if (chekedPossibilities.includes(lowerCase))
        for (i = 0; i < lowerCase.length; i++) {
            chekedPossibilities.splice(chekedPossibilities.indexOf(lowerCase[i]), 1)
        }
    if (chekedPossibilities.includes(upperCase))
        for (i = 0; i < upperCase.length; i++) {
            chekedPossibilities.splice(chekedPossibilities.indexOf(upperCase[i]), 1)
        }
    if (chekedPossibilities.includes(symbol))
        for (i = 0; i < symbol.length; i++) {
            chekedPossibilities.splice(chekedPossibilities.indexOf(symbol[i]), 1)
        }
    if (chekedPossibilities.includes(number))
        for (i = 0; i < number.length; i++) {
            chekedPossibilities.splice(chekedPossibilities.indexOf(number[i]), 1)
        }    
    
    // Second we checked if the list is check and then we add the list to possibilities
    if (chkLowercase.checked && !chekedPossibilities.includes(lowerCase))
        chekedPossibilities.push(lowerCase)
    if (chkUppercase.checked && !chekedPossibilities.includes(upperCase))
        chekedPossibilities.push(upperCase)
    if (chkSymbol.checked && !chekedPossibilities.includes(symbol))
        chekedPossibilities.push(symbol)
    if (chkNumber.checked && !chekedPossibilities.includes(number))
        chekedPossibilities.push(number)
}

function Start(){
    checkStatus()
    let possibilities = []
    for(i = 0; i < chekedPossibilities.length; i++)
        for(j = 0; j < chekedPossibilities[i].length; j++)
            possibilities.push(chekedPossibilities[i][j])
    
    console.log(product(possibilities))
}

btnStart.addEventListener("click", Start)