const express = require('express');
const router = express.Router();
const bruteForceRoute = require('./brute-force') 

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'PSN' });
});

/* GET brute force page. */
router.route('/brute-force')
  .get(bruteForceRoute.showPage);

/* GET collection page. */
router.get('/collection', function(req, res, next) {
  res.render('collection', { title: 'PSN' });
});

module.exports = router;
